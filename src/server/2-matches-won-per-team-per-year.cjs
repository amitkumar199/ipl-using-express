function matches_won_per_team_per_year(matches_data, deliveries_data) {
    let result = matches_data.reduce((accumulator, match) => {

        let season = match.season;
        let winner = match.winner;

        if (season === undefined) {
            return accumulator;
        }
        if (!accumulator.hasOwnProperty(season)) {
            accumulator[season] = {};
        }
        else {
            if (accumulator[season].hasOwnProperty(winner)) {
                accumulator[season][winner] += 1;
            }
            else {
                accumulator[season][winner] = 1;
            }
        }

        return accumulator;

    }, {});
    
    return result;
};

module.exports = matches_won_per_team_per_year;