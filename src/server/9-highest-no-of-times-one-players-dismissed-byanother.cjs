function highest_players_dismissed_another(matches_data, deliveries_data) {
  const dismissalRecords = deliveries_data.reduce((accumulator, delivery) => {

    const batsman = delivery.batsman;
    const bowler = delivery.bowler;
    const dismissal_kind = delivery.dismissal_kind;

    if (dismissal_kind !== '' && dismissal_kind && batsman && bowler) {
      if (!accumulator.hasOwnProperty(batsman)) {
        accumulator[batsman] = {};
      }

      if (!accumulator[batsman].hasOwnProperty(bowler)) {
        accumulator[batsman][bowler] = 0;
      }

      accumulator[batsman][bowler] = accumulator[batsman][bowler] + 1;
    }

    return accumulator;

  }, {});

  let highestDismissals = 0;
  let batsmanWithMostDismissals = null;
  let bowlerWithMostDismissals = null;

  for (const batsman in dismissalRecords) {
    for (const bowler in dismissalRecords[batsman]) {
      const dismissalCount = dismissalRecords[batsman][bowler];
      if (dismissalCount > highestDismissals) {
        highestDismissals = dismissalCount;
        batsmanWithMostDismissals = batsman;
        bowlerWithMostDismissals = bowler;
      }
    }
  }

  let result = {
    batsman: batsmanWithMostDismissals,
    bowler: bowlerWithMostDismissals,
    dismissals: highestDismissals
  };

  return result;
};

module.exports = highest_players_dismissed_another;