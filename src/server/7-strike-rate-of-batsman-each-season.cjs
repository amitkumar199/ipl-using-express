function batsman_strike_record_per_season(matches_data, deliveries_data) {
    let season_batsman_record = deliveries_data.reduce((accumulator, delivery) => {

        let batsman = delivery.batsman;
        const batsman_runs = parseInt(delivery.batsman_runs);

        let season_middle = matches_data.find((match) => {
            return match.id === delivery.match_id
        });

        let season = season_middle ? season_middle.season : "";

        if (!accumulator.hasOwnProperty(season)) {
            accumulator[season] = {};
        }
        if (!accumulator[season].hasOwnProperty(batsman)) {
            accumulator[season][batsman] = {
                runs: batsman_runs,
                ball_faced: 1
            };
        }else {

            accumulator[season][batsman].runs += batsman_runs;
            accumulator[season][batsman].ball_faced += 1;
        }

        return accumulator;

    }, {});

    let batsman_strike_record = Object.entries(season_batsman_record).reduce((accumulator, [season, batsman_data]) => {

        accumulator[season] = {};

        for (batsman_record in batsman_data) {
            let { runs, ball_faced } = batsman_data[batsman_record];
            let strke_rate = (runs / ball_faced) * 100;
            accumulator[season][batsman_record] = strke_rate.toFixed(2);
        }

        return accumulator;
    }, {});
    
    return batsman_strike_record;
};

module.exports = batsman_strike_record_per_season;