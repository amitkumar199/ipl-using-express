function toss_team_winner(matches_data, deliveries_data) {
    let result = matches_data.reduce((accumulator, match) => {

        let toss_winner = match.toss_winner;
        let match_winner = match.winner;

        if (match.season === undefined) {
            return accumulator;
        }
        if (toss_winner === match_winner) {
            if (!accumulator.hasOwnProperty(toss_winner)) {
                accumulator[toss_winner] = 1;
            }
            else {
                accumulator[toss_winner] += 1;
            }
        }

        return accumulator;

    }, {});

    return result;
};

module.exports = toss_team_winner; 