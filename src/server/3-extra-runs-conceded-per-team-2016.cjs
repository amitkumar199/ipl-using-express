function extra_runs_per_team(matches_data, deliveries_data) {
    let matches_data_id = matches_data
        .filter((match) => {
            return match.season === '2016';
        })
        .map((match) => {
            return match.id
        });

    let result = deliveries_data.reduce((accumulator, delivery) => {

        if (matches_data_id.includes(delivery.match_id)) {
            const bowling_team = delivery.bowling_team;
            let extra_runs = parseInt(delivery.extra_runs);

            if (!accumulator.hasOwnProperty(bowling_team)) {
                accumulator[bowling_team] = 0;
            }
            else {
                accumulator[bowling_team] += extra_runs;

            }
        }

        return accumulator;

    }, {});
    
    return result;
};

module.exports = extra_runs_per_team;