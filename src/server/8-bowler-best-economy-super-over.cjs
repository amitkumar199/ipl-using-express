function super_over_economoy_rate_bowler(matches_data, deliveries_data) {
    let super_over = deliveries_data.filter((delivery) => {

        return delivery.is_super_over === '1';
    });

    let bowler_record = super_over.reduce((accumulator, delivery) => {

        let bowler = delivery.bowler;
        let total_runs = parseInt(delivery.total_runs);

        if (delivery.wide_runs === '0' && delivery.noball_runs === '0') {
            if (!accumulator.hasOwnProperty(bowler)) {
                accumulator[bowler] = {
                    runs: 0,
                    balls: 0
                };
            }
            else {
                accumulator[bowler].runs += total_runs;
                accumulator[bowler].balls += 1;
            }
        }

        return accumulator;

    }, {});

    let bowler_record_economy_super_over = Object.keys(bowler_record).map((bowler) => {

        let { runs, balls } = bowler_record[bowler];
        let economy = (runs / balls) / 6;

        return { bowler, economy };
    });

    return bowler_record_economy_super_over;
};

module.exports = super_over_economoy_rate_bowler;