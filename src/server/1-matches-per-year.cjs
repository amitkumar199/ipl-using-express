function match_per_year(matches_data, deliveries_data) {
    let result = matches_data.reduce((accumulator, match) => {

        let season = match.season;

        if (season === undefined) {
            return accumulator;
        }
        if (accumulator.hasOwnProperty(season)) {
            accumulator[season] += 1;
        }
        else {
            accumulator[season] = 1;
        }

        return accumulator;

    }, {});

    return result;
};

module.exports = match_per_year;
