function season_no_of_players_of_match(matches_data, deliveries_data) {
    let season_wise_data = matches_data.reduce((accumulator, match) => {

        let season = match.season;
        let player_of_match = match.player_of_match;

        if (season === undefined || player_of_match === '') {
            return accumulator;
        }

        if (!accumulator.hasOwnProperty(season)) {
            accumulator[season] = {};
        }
        else {
            if (!accumulator[season].hasOwnProperty(player_of_match)) {
                accumulator[season][player_of_match] = 1;
            }
            else {
                accumulator[season][player_of_match]++;
            }
        }

        return accumulator;

    }, {});

    let result = Object.keys(season_wise_data).map((season) => {

        const players = season_wise_data[season];

        const season_player_top = Object.keys(players).sort((first_player, second_player) => {
            return players[second_player] - players[first_player]

        })[0];

        return { [season]: season_player_top };
    });

    return result;
};

module.exports = season_no_of_players_of_match;