const express = require('express');
const app = express();
const fs = require('fs');
const Papa = require('papaparse');

const match_per_year = require('./src/server/1-matches-per-year.cjs');
const matchesWonPerTeamPerYear = require('./src/server/2-matches-won-per-team-per-year.cjs');
const extrasConcededPerTeam2016 = require('./src/server/3-extra-runs-conceded-per-team-2016.cjs');
const bowlersTopEconomical2015 = require('./src/server/4-top-10-economical-bowler-2015.cjs');
const tossWinsAndMatchWins = require('./src/server/5-number-of-times-each-team-won-the-toss-and-also-won-the-match.cjs');
const playersTopPlayerOfTheMatchPerSeason = require('./src/server/6-player-won-highest-number-of-POM-awards-each-season.cjs');
const playersStrikeRatePerSeason = require('./src/server/7-strike-rate-of-batsman-each-season.cjs');
const playersMostDismissals = require('./src/server/9-highest-no-of-times-one-players-dismissed-byanother.cjs');
const bowlerBestEconomySuperOver = require('./src/server/8-bowler-best-economy-super-over.cjs');

const port = process.env.PORT || 9000;

const deliveries = fs.readFileSync("./src/data/deliveries.csv", 'utf8');
const matches = fs.readFileSync("./src/data/matches.csv", 'utf8');
const deliveries_data = Papa.parse(deliveries, { header: true }).data;
const matches_data = Papa.parse(matches, { header: true }).data;


app.get('/', (req, res) => {
    let paths = [
        '/matchesPerYear',
        '/matchesWonPerTeamPerYear',
        '/extrasConcededPerTeam2016',
        '/bowlersTopEconomical2015',
        '/tossWinsAndMatchWins',
        '/playersTopPlayerOfTheMatchPerSeason',
        '/playersStrikeRatePerSeason',
        '/playersMostDismissals',
        '/bowlerBestEconomySuperOver'
    ];
    res.status(200).send(paths);
});

app.get('/matchesPerYear', (req, res) => {
    const result = match_per_year(matches_data, deliveries_data);
    res.status(200).send(JSON.stringify(result));
});

app.get('/matchesWonPerTeamPerYear', (req, res) => {
    const result = matchesWonPerTeamPerYear(matches_data, deliveries_data);
    res.status(200).send(JSON.stringify(result));
});

app.get('/extrasConcededPerTeam2016', (req, res) => {
    const result = extrasConcededPerTeam2016(matches_data, deliveries_data);
    res.json(result);
});

app.get('/bowlersTopEconomical2015', (req, res) => {
    const result = bowlersTopEconomical2015(matches_data, deliveries_data);
    res.json(result);
});

app.get('/tossWinsAndMatchWins', (req, res) => {
    const result = tossWinsAndMatchWins(matches_data, deliveries_data);
    res.json(result);
});

app.get('/playersTopPlayerOfTheMatchPerSeason', (req, res) => {
    const result = playersTopPlayerOfTheMatchPerSeason(matches_data, deliveries_data);
    res.json(result);
});

app.get('/playersStrikeRatePerSeason', (req, res) => {
    const result = playersStrikeRatePerSeason(matches_data, deliveries_data);
    res.json(result);
});

app.get('/playersMostDismissals', (req, res) => {
    const result = playersMostDismissals(matches_data, deliveries_data);
    res.json(result);
});

app.get('/bowlerBestEconomySuperOver', (req, res) => {
    const result = bowlerBestEconomySuperOver(matches_data, deliveries_data);
    res.json(result);
});

app.listen(port, () => {
    console.log(`listening ${port}`);
});